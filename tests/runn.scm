(import test)
(include "main.scm")

(test-begin)
(test-group "helpers"
  (test-group "the repeat procedure"
    (test-group "should return a list"
      (test '() (repeat "whatever" 0))
      (test '() (repeat 'whatever 0))
      (test '() (repeat 42 0)))
    (test-group "should return the second argument in a list"
      (test '("whatever") (repeat "whatever" 1))
      (test '(whatever) (repeat 'whatever 1))
      (test '(42) (repeat 42 1)))
    (test-group "should repeat the argument the right amount of time"
      (test '(42 42) (repeat 42 2))
      (test '(42 42 42 42) (repeat 42 4))
      (test '() (repeat 42 -1))))
  (test-group "the intercalate procedure"
	(test "should return an empty string" "" (intercalate #\space '()))
	(test-group "should add sep in between"
	  (test "hello world" (intercalate #\space '("hello" "world")))
    (test "car,cdr" (intercalate #\, '("car" "cdr")))
    (test "crameur\nde\nfils" (intercalate #\newline '("crameur" "de" "fils")))
    )))

(test-group "the game"
  (test-group "the init-grid procedure"
    (test-group "should generate proper grids filled with #f"
      (test '((#f)) (init-grid (cons 1 1)))
      (test '((#f #f)) (init-grid (cons 2 1)))
      (test '((#f #f) (#f #f)) (init-grid (cons 2 2)))
      (test '() (init-grid (cons 0 0)))))

  (test-group "the show-minefield procedure"
    (test-group "returns a formatted string representing the grid"
      (test "" (show-minefield '(())))
      (test "o" (show-minefield '((#f))))
	  (test "oo" (show-minefield '((#f #f))))
	  (test "oo\noo" (show-minefield '((#f #f) (#f #f)))))))
(test-end)
