(import (srfi-1)
        (bindings)
        (pipes))

(define (init-grid size)
  (bind (width . height) size
    (pipe #f
      (repeat width)
      (repeat height))))

(define (show-minefield grid)
  (let ((show-line (lambda (line)
					 (fold (lambda (a b) (string-append b "o")) "" line))))
    (car (map show-line grid))))
  

(define (intercalate sep words)
      (fold (lambda (a b)
              (if (equal? "" b)
                  a
                  (string-append b (list->string (list sep)) a)))
            "" 
            words))

(: repeat ('a integer -> (list-of 'a)))
(define (repeat x n)
  (if (>= 0 n)
      '()
      (cons x (repeat x (- n 1)))))
